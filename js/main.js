
$(window).on('load', function () {
  // Before after affect photo
  $(".item__images").twentytwenty({
    default_offset_pct: 0.7,
    hover: false,
    move_with_handle_only: true
  });

});




//show more text


//Get the current year for the copyright
$(".year").text(new Date().getFullYear());





//navbar handler
function navHandler() {
  var x = document.getElementById("myTopnav");
  if (x.className === "navbar__links") {
    x.className += " responsive";
  } else {
    x.className = "navbar__links";
  }
}

//Slick slider testimonials
$(".testimonials__items").slick({
  dots: true,
  // ,
  // draggable: true,
  prevArrow:
    '<div class="arrow__left"><span class="fa fa-angle-left"></span><span class="sr-only">Prev</span></div>',
  nextArrow:
    '<div class="arrow__right"><span class="fa fa-angle-right"></span><span class="sr-only">Next</span></div>',
  autoplay: true,
  autoplaySpeed: 3000
});

// Button handler
document.querySelectorAll(".to__contact-page").forEach(function (element) {
  element.addEventListener("click", function () {
    document.location.href = 'contacts.html';
  });
})

document.querySelectorAll(".to__project-page").forEach(function (element) {
  element.addEventListener("click", function () {
    document.location.href = 'projecten.html';
  });
})

document.querySelectorAll(".to__services-page").forEach(function (element) {
  element.addEventListener("click", function () {
    document.location.href = 'diensten.html';
  });
})

function totaalRenotationProject() {
  document.location.href = 'renovatie-1.html';

}

//form handler
var form = document.getElementById('home-page__form'),
  url = 'https://script.google.com/macros/s/AKfycbyT9TEGG0FsgdjnhC4sTkCdyMDkzZjkKOq9Nwjw69WxWwlIBMpX/exec'


form.addEventListener('submit', e => {
  console.log(form)
  e.preventDefault()
  fetch(url, { method: 'POST', body: new FormData(form) })
    .then(response => console.log('Success!', response)

    )
    .catch(error => console.error('Error!', error.message))

  $('#thankyou_message').css("display", "inline-block");
  document.getElementById('phone').value = '';
  document.getElementById('name').value = '';
  document.getElementById('message').value = '';
})


// Smooth scrolling
$("#arrow-link").on("click", function (e) {
  // e.preventDefault();

  var target = this.hash,
    $target = $(target);

  $("html, body")
    .stop()
    .animate(
      {
        scrollTop: $target.offset().top - 100,
      },
      800,
      "swing",
      function () {
        window.location.hash = target;
      }
    );
});





